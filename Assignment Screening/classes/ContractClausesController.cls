public class ContractClausesController {
	public class ContractClausesException extends Exception {}
    @AuraEnabled
    public static List<Contract_Clause_Junction__c> getContractClauseJunctions(Id ContractId){
        List<Contract_Clause_Junction__c> CCJL = [SELECT Id FROM Contract_Clause_Junction__c WHERE Contract__c = :ContractId];
        if(CCJL.size()==0){
            throw new ContractClausesException('The Contract Clause Junction List is empty!');
        }
        return CCJL;
    }
    @AuraEnabled
    public static void removeCCJID(id CCJId){
        Database.DeleteResult dr = Database.delete(CCJId);
        /*if(dr.isSuccess()==false){
            throw new ContractClausesException('The Contract Clause Junction was not deleted!');
        }*/
    }
}
public class ModalComponentController {
	public class ModalComponentException extends Exception {}
    @AuraEnabled
    public static void createClauses(id contractid, List<id> clauseList){
        if(clauseList.size()==0){
            throw new ModalComponentException('The List of Clause IDs is empty!');
        }
        List<Contract_Clause_Junction__c> CCJ = new List<Contract_Clause_Junction__c>();
        for(Integer i = 0; i < clauseList.size(); i++){
            Contract_Clause_Junction__c a = new Contract_Clause_Junction__c(Contract__c = contractid, Contract_Clause__c = clauseList[i]);
        	CCJ.add(a);
        }
        database.insert(CCJ);
    }
}
public class ShowContractsController {
    public class ControllerException extends Exception {}
	@AuraEnabled
    public static List<Contract> getContractsList(){	//init function for ShowContracts.cmp
        List<Contract> CL = [SELECT Id FROM Contract];
        if(CL.size()==0){
            throw new ControllerException('There are no Contracts!');
        }
        return CL;
    }
}
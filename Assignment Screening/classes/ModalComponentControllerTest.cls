@isTest
public class ModalComponentControllerTest {
    @isTest static void testCreateClauses(){
        Contract a = new Contract(Status='Draft', AccountId='0014600001T5r5uAAB', StartDate=date.today(), ContractTerm=6);
        insert a;
        Contract_Clause__c cc1 = new Contract_Clause__c(Name='Test Clause1', Description__c='This is only for testing purposes',
                                                      Clause_Type__c='Other');
        Contract_Clause__c cc2 = new Contract_Clause__c(Name='Test Clause2', Description__c='This is only for testing purposes',
                                                      Clause_Type__c='Other');
        Contract_Clause__c cc3 = new Contract_Clause__c(Name='Test Clause3', Description__c='This is only for testing purposes',
                                                      Clause_Type__c='Other');
 		list<Contract_Clause__c> cc = new list<Contract_Clause__c>();
        cc.add(cc1);
        cc.add(cc2);
        cc.add(cc3);
        insert cc;
        list<Id> CCLTest = new list<Id>();
        CCLTest.add(cc1.Id);
        CCLTest.add(cc2.Id);
        CCLTest.add(cc3.Id);
        ModalComponentController.createClauses(a.Id, CCLTest);
        List<Contract_Clause_Junction__c> CCJL = [SELECT Id FROM Contract_Clause_Junction__c WHERE Contract__c = :a.Id];
        system.assert(CCJL.size()>0, 'The clauses were inserted and pulled back with SOQL!');
    }
    @isTest static void negtestCreateClauses(){
        Contract a = new Contract(Status='Draft', AccountId='0014600001T5r5uAAB', StartDate=date.today(), ContractTerm=6);
        insert a;
        list<Id> EmptyCCL = new list<Id>();
        Boolean caughtBool = false;
        try{
            ModalComponentController.createClauses(a.Id, EmptyCCL);
        }
        catch(ModalComponentController.ModalComponentException MCE){
            caughtbool = true;
        }        
        system.assert(caughtBool, 'Properly caught Modal Component controller exception!');
    }
}
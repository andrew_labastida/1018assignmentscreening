@isTest
public class ShowContractsControllerTest {
    @isTest static void testgetContractsList(){
        Contract a = new Contract(Status='Draft', AccountId='0014600001T5r5uAAB', StartDate=date.today(), ContractTerm=6);
        Contract b = new Contract(Status='Draft', AccountId='0014600001T5r5uAAB', StartDate=date.today(), ContractTerm=6);
        Contract c = new Contract(Status='Draft', AccountId='0014600001T5r5uAAB', StartDate=date.today(), ContractTerm=6);
        List<Contract> CL = new List<Contract>();
        CL.add(a);
        CL.add(b);
        CL.add(c);
        insert CL;
        List<Contract> testCL = ShowContractsController.getContractsList();
        system.assert(testCL.size()>=1, 'TestCL should not be empty!');
    }
    @isTest static void negtestgetContractsList(){
        Boolean caughtBool = false;
        try{
            List<Contract> testCL = ShowContractsController.getContractsList();   
        }
        catch(ShowContractsController.ControllerException CE){
            caughtbool = true;
        }        
        system.assert(caughtBool, 'Properly caught controller exception!');
    }
}
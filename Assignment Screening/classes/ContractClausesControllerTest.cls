@isTest
public class ContractClausesControllerTest {
    @isTest static void testgetContractClauseJunctions(){
        Contract a = new Contract(Status='Draft', AccountId='0014600001T5r5uAAB', StartDate=date.today(), ContractTerm=6);
        insert a;
        Contract_Clause__c cc = new Contract_Clause__c(Name='Test Clause', Description__c='This is only for testing purposes',
                                                      Clause_Type__c='Other');
        insert cc;
        Contract_Clause_Junction__c ccj = new Contract_Clause_Junction__c(Contract__c = a.Id, Contract_Clause__c = cc.Id);
        insert ccj;
        List<Contract_Clause_Junction__c> TestCCJL = ContractClausesController.getContractClauseJunctions(a.Id);
        system.assert(TestCCJL.size()>0,'The List is populated');
    }
    @isTest static void negtestgetContractClauseJunctions(){
        Contract a = new Contract(Status='Draft', AccountId='0014600001T5r5uAAB', StartDate=date.today(), ContractTerm=6);
        insert a;
        Boolean caughtBool = false;
        try{
            List<Contract_Clause_Junction__c> testCCJL = ContractClausesController.getContractClauseJunctions(a.Id);   
        }
        catch(ContractClausesController.ContractClausesException CCE){
            caughtbool = true;
        }        
        system.assert(caughtBool, 'Properly caught Contract Clause controller exception!');
    }
    
    @isTest static void testremoveCCJID(){
        Contract a = new Contract(Status='Draft', AccountId='0014600001T5r5uAAB', StartDate=date.today(), ContractTerm=6);
        insert a;
        Contract_Clause__c cc = new Contract_Clause__c(Name='Test Clause', Description__c='This is only for testing purposes',
                                                      Clause_Type__c='Other');
        insert cc;
        Contract_Clause_Junction__c ccj = new Contract_Clause_Junction__c(Contract__c = a.Id, Contract_Clause__c = cc.Id);
        insert ccj;
        
        ContractClausesController.removeCCJID(ccj.Id);
         Boolean caughtBool = false;
        try{
            List<Contract_Clause_Junction__c> testCCJL = ContractClausesController.getContractClauseJunctions(a.Id);   
        }
        catch(ContractClausesController.ContractClausesException CCE){
            caughtbool = true;
        }        
        system.assert(caughtBool, 'Properly caught Contract Clause controller exception for the removeCCJID!');
    }
   
}
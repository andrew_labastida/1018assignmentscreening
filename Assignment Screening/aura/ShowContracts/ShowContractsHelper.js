({
	fetchContracts : function(component, event, helper) {
		var action = component.get("c.getContractsList");
        action.setCallback(this, function(response){
            var state = response.getState();
            // state can be success, error, or incomplete
            if(state === "SUCCESS"){
                var ContractList = response.getReturnValue();
                component.set("v.ContractList", ContractList);
            } 
            else{
                alert("Error!");
            }
        });
        $A.enqueueAction(action);
    }
    
})
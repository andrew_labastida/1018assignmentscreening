({
	 // Function used to open the contact modal
    openClauseModal: function(component, event, helper) {
        var modal = component.find("ClauseModal");
        var modalBackdrop = component.find("ClauseModalBackdrop");
        $A.util.addClass(modal,"slds-fade-in-open");
        $A.util.addClass(modalBackdrop,"slds-backdrop_open");
    },

    // Function used to close the contact modal
    closeClauseModal: function(component, event, helper) {
        var modal = component.find("ClauseModal");
        var modalBackdrop = component.find("ClauseModalBackdrop");
        $A.util.removeClass(modal,"slds-fade-in-open");
        $A.util.removeClass(modalBackdrop,"slds-backdrop_open");
    },

    // Function used to create new contact
    createClause: function(component, event, helper) {
        helper.insertClause(component, event, helper);
    },
    storeCheckBoxData: function(component, event, helper){
    	var changeValue = event.getParam("value");
        console.log(changeValue);
        component.set("v.checkBoxData", changeValue);
	}
})
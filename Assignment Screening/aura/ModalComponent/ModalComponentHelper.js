({
	insertClause: function(component, event, helper) {           
    	var createAction = component.get("c.createClauses");
        var contid = component.get("v.contractID");
        console.log(contid);
        var checkdata = component.get("v.checkBoxData");
        console.log(checkdata);
        createAction.setParams({
        	contractid: contid,
            clauseList: checkdata
        });
        createAction.setCallback(this, function(response) {           
        	// Getting the state from response
            var state = response.getState();
            if(state === 'SUCCESS') {
            	window.location.reload();//CHANGE LATER??????????
            	//alert('GREAT SUCCESS!');
            } 
            else {
                // Show an alert if the state is incomplete or error
                alert('Error in getting data');
            }
        });
        // Adding the action variable to the global action queue
        $A.enqueueAction(createAction);
    }
})
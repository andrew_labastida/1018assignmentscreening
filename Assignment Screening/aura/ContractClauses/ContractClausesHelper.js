({
	 fetchCCJL : function(component, event, helper) {
		var action = component.get("c.getContractClauseJunctions");
        var contractID = component.get("v.ContractId");//get attribute
        action.setParams({
            ContractId: contractID  
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                var CCJL = response.getReturnValue();
            	//console.log(CCJL);
                component.set("v.CCJL", CCJL);
            } 
            else{
                alert("A related list may be empty!");
            }
        });
        $A.enqueueAction(action);
    },
    removeCCJ : function(component, event, helper) {
		var action = component.get("c.removeCCJID");
        var ccjid = event.getSource().get("v.value");
        //console.log("Currently in helper ");
        //console.log(ccjid);
        action.setParams({
            CCJId: ccjid
        });
        action.setCallback(this, function(response){
            var state = response.getState();     
            if(state === "SUCCESS"){
                window.location.reload();
               // $A.get("e.force:refreshView").fire();
            } 
            else{
                alert("removeCCJ Error!");
            }
            //this.fetchCCJL(component);
        });
        $A.enqueueAction(action);
    }
})